package controlador;
import modelo.Docente;
import vista.dlgNomina;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JFrame;

public class Controlador implements ActionListener{
    private Docente docente;
    private dlgNomina vista;

    public Controlador(Docente docente, dlgNomina vista) {
        this.docente = docente;
        this.vista = vista;
        
        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnGuardar.addActionListener(this);
        this.vista.btnLimpiar.addActionListener(this);
        this.vista.btnCancelar.addActionListener(this);
        this.vista.btnCerrar.addActionListener(this);
        this.vista.btnMostrar.addActionListener(this);
    }
    
    public void iniciarVista(){
        this.vista.setTitle("Nomina");
        this.vista.setSize(550, 450);
        this.vista.setVisible(true);
    }
    
    public void limpiar(){
        this.vista.txtNumDocente.setText("");
        this.vista.txtNombre.setText("");
        this.vista.txtDomicilio.setText("");
        this.vista.cbNivel.setSelectedIndex(0);
        this.vista.txtPagoBase.setText("");
        this.vista.txtHoras.setText("");
    }
    
    public boolean isVacio(){
       if(this.vista.txtNumDocente.getText().equals("" ) || this.vista.txtNombre.getText().equals("" ) || this.vista.txtDomicilio.getText().equals("" ) || this.vista.cbNivel.getSelectedIndex()==0 || this.vista.txtPagoBase.getText().equals("" ) || this.vista.txtHoras.getText().equals("" )){
           return true;
       }
       else{
           return false;
       }
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        if(e.getSource()==this.vista.btnNuevo){
            this.vista.btnGuardar.setEnabled(true);
            this.vista.txtNumDocente.enable(true);
            this.vista.txtNombre.enable(true);
            this.vista.txtDomicilio.enable(true);
            this.vista.cbNivel.setEnabled(true);
            this.vista.txtPagoBase.enable(true);
            this.vista.txtHoras.enable(true);
            this.vista.spHijos.setEnabled(true);
        }
        
        if(e.getSource()==this.vista.btnLimpiar){
            limpiar();
        }
        
        if(e.getSource()==this.vista.btnCancelar){
            limpiar();
            this.vista.btnGuardar.setEnabled(false);
            this.vista.txtNumDocente.enable(false);
            this.vista.txtNombre.enable(false);
            this.vista.txtDomicilio.enable(false);
            this.vista.cbNivel.setEnabled(false);
            this.vista.txtPagoBase.enable(false);
            this.vista.txtHoras.enable(false);
        }
        
        if(e.getSource()==this.vista.btnGuardar){
            
            if(isVacio()==true){
                JOptionPane.showMessageDialog(vista, "No se puede guardar debido a campos vacios");
            }
            else{
                try{
                    this.docente.setNumDocente(Integer.parseInt(this.vista.txtNumDocente.getText()));
                    this.docente.setNombre(this.vista.txtNombre.getText());
                    this.docente.setDomicilio(this.vista.txtDomicilio.getText());
                    this.docente.setNivel((this.vista.cbNivel.getSelectedIndex()));
                    this.docente.setPagoBase(Float.parseFloat(this.vista.txtPagoBase.getText()));
                    this.docente.setHorasImpartidas(Integer.parseInt(this.vista.txtHoras.getText()));
                    JOptionPane.showMessageDialog(vista, "GUARDADO CON EXITO");
                    this.vista.btnMostrar.setEnabled(true);
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ ex.getMessage());
                }
            }
            
        }
        
        if(e.getSource()==this.vista.btnMostrar){
            this.vista.txtPagoHoras.setText(String.valueOf(this.docente.calcularPago()));
            this.vista.txtBono.setText(String.valueOf(this.docente.calcularBono((int)this.vista.spHijos.getValue())));
            this.vista.txtImpuesto.setText(String.valueOf(this.docente.calcularImpuesto()));
            this.vista.txtTotal.setText(String.valueOf((this.docente.calcularPago()+this.docente.calcularBono((int)this.vista.spHijos.getValue())-this.docente.calcularImpuesto())));
        }
        
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista, "¿Desea Cerrar el programa?",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
    }
    
    public static void main(String[] args) {
        Docente docente = new Docente();
        dlgNomina nomina = new dlgNomina(new JFrame(), true);
        Controlador contra= new Controlador(docente, nomina);
        contra.iniciarVista();
    }
}

/* Si los commit fueron tan rapidos fue porque tuve que hacer un nuevo proyecto 
   porque en el examen trabaje en una maquina de la escuela en la de mi casa
   ya no me dejo debido a que no era la misma maquina

    Quezada Ramos Julio Emiliano 2019030880
    PROYECTO FINALIZADO
*/