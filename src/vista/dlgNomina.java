/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

/**
 *
 * @author Tec. Móvil
 */
public class dlgNomina extends javax.swing.JDialog {

    /**
     * Creates new form dlgNomina
     */
    public dlgNomina(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        spHijos = new javax.swing.JSpinner();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtPagoHoras = new javax.swing.JLabel();
        txtBono = new javax.swing.JLabel();
        txtImpuesto = new javax.swing.JLabel();
        txtTotal = new javax.swing.JLabel();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        txtNumDocente = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDomicilio = new javax.swing.JTextField();
        cbNivel = new javax.swing.JComboBox<>();
        txtPagoBase = new javax.swing.JTextField();
        txtHoras = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setText("Num. Docente");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(10, 10, 80, 20);

        jLabel2.setText("Nombre");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(10, 40, 60, 20);

        jLabel3.setText("Domicilio");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(10, 70, 70, 20);

        jLabel4.setText("Nivel");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(10, 100, 60, 20);

        jLabel5.setText("Pago por Hora Base");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(10, 130, 160, 20);

        jLabel6.setText("Horas Impartidas");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(10, 160, 100, 20);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos De Pago", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N
        jPanel1.setLayout(null);

        jLabel7.setText("Pago por horas impartidas                  $");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 25, 210, 16);

        jLabel8.setText("Hijos");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(10, 55, 40, 20);

        spHijos.setEnabled(false);
        jPanel1.add(spHijos);
        spHijos.setBounds(40, 55, 64, 22);

        jLabel9.setText("Pago por Bono     $");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(110, 50, 110, 30);

        jLabel10.setText("Descuento por Impuesto                     $");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(10, 85, 240, 16);

        jLabel11.setText("Total a Pagar                                        $");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(10, 115, 220, 16);
        jPanel1.add(txtPagoHoras);
        txtPagoHoras.setBounds(220, 25, 90, 20);
        jPanel1.add(txtBono);
        txtBono.setBounds(220, 60, 90, 20);
        jPanel1.add(txtImpuesto);
        txtImpuesto.setBounds(220, 90, 80, 20);
        jPanel1.add(txtTotal);
        txtTotal.setBounds(220, 115, 90, 20);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 200, 500, 140);

        btnLimpiar.setText("Limpiar");
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(0, 380, 100, 50);

        btnCancelar.setText("Cancelar");
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(195, 380, 100, 50);

        btnCerrar.setText("Cerrar");
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(400, 380, 100, 50);

        jPanel2.setBackground(new java.awt.Color(255, 255, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Acciones", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 0, 14))); // NOI18N
        jPanel2.setLayout(null);

        btnNuevo.setText("Nuevo");
        btnNuevo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 255, 102), 2));
        jPanel2.add(btnNuevo);
        btnNuevo.setBounds(15, 30, 100, 30);

        btnGuardar.setText("Guardar");
        btnGuardar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 255, 102), 2));
        btnGuardar.setEnabled(false);
        jPanel2.add(btnGuardar);
        btnGuardar.setBounds(15, 80, 100, 30);

        btnMostrar.setText("Mostrar");
        btnMostrar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 255, 102), 2));
        btnMostrar.setEnabled(false);
        jPanel2.add(btnMostrar);
        btnMostrar.setBounds(15, 130, 100, 30);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(370, 0, 130, 180);

        txtNumDocente.setEnabled(false);
        txtNumDocente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumDocenteActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumDocente);
        txtNumDocente.setBounds(130, 10, 100, 20);

        txtNombre.setEnabled(false);
        getContentPane().add(txtNombre);
        txtNombre.setBounds(130, 40, 100, 20);

        txtDomicilio.setEnabled(false);
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(130, 70, 100, 20);

        cbNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select", "Licenciatura", "Maestria", "Doctorado" }));
        cbNivel.setEnabled(false);
        getContentPane().add(cbNivel);
        cbNivel.setBounds(130, 100, 100, 22);

        txtPagoBase.setEnabled(false);
        getContentPane().add(txtPagoBase);
        txtPagoBase.setBounds(130, 130, 100, 20);

        txtHoras.setEnabled(false);
        getContentPane().add(txtHoras);
        txtHoras.setBounds(130, 160, 100, 20);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNumDocenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumDocenteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumDocenteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgNomina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgNomina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgNomina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgNomina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgNomina dialog = new dlgNomina(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnCancelar;
    public javax.swing.JButton btnCerrar;
    public javax.swing.JButton btnGuardar;
    public javax.swing.JButton btnLimpiar;
    public javax.swing.JButton btnMostrar;
    public javax.swing.JButton btnNuevo;
    public javax.swing.JComboBox<String> cbNivel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JSpinner spHijos;
    public javax.swing.JLabel txtBono;
    public javax.swing.JTextField txtDomicilio;
    public javax.swing.JTextField txtHoras;
    public javax.swing.JLabel txtImpuesto;
    public javax.swing.JTextField txtNombre;
    public javax.swing.JTextField txtNumDocente;
    public javax.swing.JTextField txtPagoBase;
    public javax.swing.JLabel txtPagoHoras;
    public javax.swing.JLabel txtTotal;
    // End of variables declaration//GEN-END:variables
}
